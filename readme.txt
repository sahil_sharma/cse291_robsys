Perform the following actions (editing code and debugging code in the online IDE to confirm compilation, and committing it to your repository). 

* All source files can be found on TED in the Assignment directory.

1.	Ensure you have a master branch (depending on how you work through these steps, one may have been created automatically).
2.	Create branch ticket-33423 by branching off of master.
3.	Create file intro.c and add the code from ticket-33423.c to it.  Add intro.c to your repository and commit it with message “Ticket 33423”.
4.	Push the ticket-33423 branch to your online repo on BitBucket.
5.	Merge branch ticket-33423 into master.
6.	Push the master branch to your online repo on BitBucket.
7.	Create branch ticket-33423-bug from master.  Locate and correct the bug in the code.  Commit the change to the ticket-33423-bug branch with message “Fixing bug from Ticket 33423”.
8.	Push the ticket-33423-bug branch to your online repo on BitBucket.
9.	Create branch ticket-55523 from master.  
10.	Edit intro.c and replace with the code in ticket-55523.c.
11.	Commit this change to ticket-55523.
12.	Push ticket-55523 branch to your online repo on BitBucket.
13.	Merge ticket-33423-bug into master using the no fast-forward flag.
14.	Merge ticket-55523 into master using the no fast-forward flag.  This results in a merge conflict.  Resolve the conflict by editing intro.c favoring the code changes from ticket-33423-bug.  Commit the code to master with message “Fixing conflict from ticket-33423-bug and ticket-55523”.
15.	Push the master branch to your online repo on BitBucket.
16.	Post a picture of the branch history (either from SourceTree or the Git command line tool) to your Assignment 0 wiki page.
17.	Create branch ticket-77233 from master.  Edit intro.c by incorporating the `uart_activity` DigitalOut from ticket-77233.c.  The behavior should be that any time a character is read from the serial channel, the digital output is toggled.  Commit the code to ticket-77233.
18.	 Push the ticket-77233 branch to your online repo on BitBucket.
19.	Create branch test from branch master.
20.	Merge ticket-77233 into test.
21.	Push the test branch to your online repo on BitBucket.
22.	Create assignment0 branch from your current branch and push it to BitBucket.
23.	We will pick up here in lab to verify your code changes on an actual MBED LPC1768 device.
